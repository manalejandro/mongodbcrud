package com.manalejandro.mongodbcrud.repositories;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import com.manalejandro.mongodbcrud.model.Item;

@Repository
public interface ItemRepository extends ReactiveMongoRepository<Item, String> {
}
