package com.manalejandro.mongodbcrud.forms;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IndexForm {

	private String id;
	private String nombre;
	private String apellido1;
	private String apellido2;

}
