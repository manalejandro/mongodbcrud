package com.manalejandro.mongodbcrud;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.aop.interceptor.SimpleAsyncUncaughtExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import com.manalejandro.mongodbcrud.model.Item;
import com.manalejandro.mongodbcrud.services.ItemService;

@SpringBootApplication
@EnableAsync
@EnableReactiveMongoRepositories(basePackages = { "com.manalejandro.mongodbcrud.repositories" })
public class MongodbcrudApplication implements CommandLineRunner, AsyncConfigurer {

	private ItemService itemService;
	private Logger logger;

	@Autowired
	public MongodbcrudApplication(ItemService itemService) {
		this.itemService = itemService;
		this.logger = LoggerFactory.getLogger(this.getClass());
	}

	public static void main(String[] args) {
		SpringApplication.run(MongodbcrudApplication.class, args);
	}

	public void run(String... args) throws InterruptedException, ExecutionException {
		final List<Item> items = new ArrayList<Item>();
		for (int i = 1; i <= 10; i++) {
			items.add(new Item("nombre_" + i, "apellido1_" + i, "apellido2_" + i));
		}
		if (itemService.saveAll(items).block(Duration.ofSeconds(5))) {
			this.logger.info("Datos de prueba cargados correctamente...");
		} else {
			this.logger.error("Error: No se han podido cargar los datos");
		}
	}

	@Bean
	public Executor getAsyncExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(7);
		executor.setMaxPoolSize(42);
		executor.setQueueCapacity(11);
		executor.setThreadNamePrefix("Thread-");
		executor.initialize();
		return executor;
	}

	@Bean
	public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
		return new SimpleAsyncUncaughtExceptionHandler();
	}
	
}