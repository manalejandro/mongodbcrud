package com.manalejandro.mongodbcrud.vo;

import java.util.ArrayList;
import java.util.List;

import com.manalejandro.mongodbcrud.model.Item;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IndexVO {

	private final List<Item> listItem = new ArrayList<Item>();
	private String id;
	private String nombre;
	private String apellido1;
	private String apellido2;

}
