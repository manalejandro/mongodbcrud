package com.manalejandro.mongodbcrud.services;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.manalejandro.mongodbcrud.model.Item;
import com.manalejandro.mongodbcrud.repositories.ItemRepository;

import reactor.core.publisher.Mono;

@Service
public class ItemServiceImpl implements ItemService {

	private ItemRepository itemRepository;

	@Autowired
	public ItemServiceImpl(ItemRepository itemRepository) {
		this.itemRepository = itemRepository;
	}

	@Async
	public List<Item> getAll() throws InterruptedException, ExecutionException {
		return CompletableFuture
				.completedFuture(this.itemRepository.findAll().collectList().block(Duration.ofSeconds(5))).get();
	}

	@Async
	public Mono<Item> getItem(String id) {
		return this.itemRepository.findById(id).block(Duration.ofSeconds(5)) != null ? Mono.fromFuture(
				CompletableFuture.completedFuture(this.itemRepository.findById(id).block(Duration.ofSeconds(5))))
				: null;
	}

	@Async
	public Mono<Boolean> save(Item item) {
		return this.itemRepository.save(item).block(Duration.ofSeconds(5)) != null
				? Mono.fromFuture(CompletableFuture.completedFuture(Boolean.TRUE))
				: Mono.fromFuture(CompletableFuture.completedFuture(Boolean.FALSE));
	}

	@Async
	public Mono<Boolean> saveAll(List<Item> items) {
		return this.itemRepository.saveAll(items).collectList().block(Duration.ofSeconds(5)) != null
				? Mono.fromFuture(CompletableFuture.completedFuture(Boolean.TRUE))
				: Mono.fromFuture(CompletableFuture.completedFuture(Boolean.FALSE));
	}

	@Async
	public void delete(Item item) {
		this.itemRepository.delete(item).block(Duration.ofSeconds(5));
	}
}
