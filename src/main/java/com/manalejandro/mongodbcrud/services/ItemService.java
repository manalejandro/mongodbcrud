package com.manalejandro.mongodbcrud.services;

import java.util.List;
import java.util.concurrent.ExecutionException;

import com.manalejandro.mongodbcrud.model.Item;

import reactor.core.publisher.Mono;

public interface ItemService {

	public Mono<Item> getItem(String id);

	public List<Item> getAll() throws InterruptedException, ExecutionException;

	public Mono<Boolean> save(Item item);

	public Mono<Boolean> saveAll(List<Item> items);

	public void delete(Item item);
}
