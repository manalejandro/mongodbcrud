package com.manalejandro.mongodbcrud.controllers;

import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.manalejandro.mongodbcrud.forms.IndexForm;
import com.manalejandro.mongodbcrud.model.Item;
import com.manalejandro.mongodbcrud.services.ItemService;
import com.manalejandro.mongodbcrud.vo.IndexVO;

@Controller
public class IndexController implements ErrorController {

	private ItemService itemService;
	private Logger logger;

	@Autowired
	public IndexController(ItemService itemService) {
		this.itemService = itemService;
		this.logger = LoggerFactory.getLogger(this.getClass());
	}

	@GetMapping({ "/", "/index" })
	public String index(final @ModelAttribute("indexForm") IndexForm indexForm, final Model model)
			throws InterruptedException, ExecutionException {
		IndexVO indexVO = new IndexVO();
		indexVO.getListItem().addAll(itemService.getAll());
		model.addAttribute("indexVO", indexVO);
		this.logger.info("GET index");
		return "index";
	}

	@GetMapping("/new")
	public String newItem(final Model model) {
		IndexForm indexForm = new IndexForm();
		model.addAttribute("indexForm", indexForm);
		this.logger.info("GET new item");
		return "new";
	}

	@PostMapping("/new")
	public String saveItem(final @ModelAttribute("indexForm") IndexForm indexForm) {
		Item item = new Item();
		item.setNombre(indexForm.getNombre());
		item.setApellido1(indexForm.getApellido1());
		item.setApellido2(indexForm.getApellido2());
		this.logger.info("POST new item: " + item.getNombre() + " " + item.getApellido1() + " " + item.getApellido2());
		if (!item.getNombre().isBlank() && !item.getApellido1().isBlank() && !item.getApellido2().isBlank()) {
			if (this.itemService.save(item).block()) {
				return "redirect:/";
			} else {
				return "redirect:/error";
			}
		} else {
			return "redirect:/error";
		}
	}

	@GetMapping("/{id}/edit")
	public String editItem(final @PathVariable String id, final Model model) {
		IndexForm indexForm = new IndexForm();
		Item item = itemService.getItem(id).block();
		this.logger.info("GET edit item: " + id);
		if (item != null) {
			indexForm.setId(item.getId());
			indexForm.setNombre(item.getNombre());
			indexForm.setApellido1(item.getApellido1());
			indexForm.setApellido2(item.getApellido2());
			model.addAttribute("indexForm", indexForm);
			return "edit";
		} else {
			return "redirect:/error";
		}
	}

	@PostMapping("/{id}/edit")
	public String saveEditItem(final @ModelAttribute("indexForm") IndexForm indexForm) {
		this.logger.info("POST edit item: " + indexForm.getId());
		if (itemService.getItem(indexForm.getId()) != null && !indexForm.getNombre().isBlank()
				&& !indexForm.getApellido1().isBlank() && !indexForm.getApellido2().isBlank()) {
			Item item = new Item();
			item.setId(indexForm.getId());
			item.setNombre(indexForm.getNombre());
			item.setApellido1(indexForm.getApellido1());
			item.setApellido2(indexForm.getApellido2());
			if (this.itemService.save(item).block()) {
				return "redirect:/";
			} else {
				return "redirect:/error";
			}
		} else {
			return "redirect:/error";
		}
	}

	@GetMapping("/{id}/delete")
	public String deleteItem(final @PathVariable String id) {
		this.logger.info("DELETE item: " + id);
		if (!id.isBlank()) {
			Item item = this.itemService.getItem(id).block();
			if (item != null) {
				this.itemService.delete(item);
				return "redirect:/";
			} else {
				return "redirect:/error";
			}
		} else {
			return "redirect:/error";
		}
	}

	@GetMapping("/error")
	public String getErrorPath() {
		this.logger.error("ERROR");
		return "error";
	}

}
